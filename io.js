const fs = require('fs');

function writeUserDataToFile(data){
  console.log("writeUserDataToFile");


  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios.json", jsonUserData,"utf8",
    function(err){
      if(err){
        console.log(err);
      }else{
        console.log("Usuario escrito en fichero");
      }
    }
);
}

//Exporto la función con el nombre que quiera, en este caso es el mismo
module.exports.writeUserDataToFile=writeUserDataToFile;
