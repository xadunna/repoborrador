#Imagen raíz
FROM node

#Carpeta raíz
WORKDIR /apitechu

#Copia de archivos de carpeta local a apitechu
ADD . /apitechu

#Instalación de las dependencias
RUN npm install --only=prod

#Puerto de trabajo
EXPOSE 3000

#Comando de inicialización
CMD ["node", "server.js"]
