//Carga la librería de de variables de entorno y carga el fichero .env
require('dotenv').config();
//fija variable que no puede cambiar; el require mira en las dependencias
const express = require('express');
//importo nuevo paquete

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

//inicializa el FWK y lo pone en la variable
const app = express();

//permite que sólo lleguen peticiones de un navegador el mismo dominio
var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 // This will be needed. Por defecto no deja pasar las cabeceras. Para tsec token jwt hay que añadir
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

//Indicamos a express que loss bodys van a ir en JSON(asignamos un preprocesador)
app.use(express.json());
app.use(enableCORS);

//si no está la variable de entorno asigna el puerto 3000
const port = process.env.PORT || 3000;
//inicializa la app
app.listen(port);

console.log("API escuchando en el puerto " + port);

//Definimos la función get, post, ...
//req recoge petición http
app.get ("/apitechu/v1/hello",
  function (req,res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg": "Hola desde API TechU"});
  }
)

//Va a contestar siempre que reciba 2 parámetros
app.post(
  "/apitechu/v1/monstruo/:p1/:p2",
    function (req,res){
      console.log("Parámetros");
      console.log(req.params);

      console.log("Query String");
      console.log(req.query);

      console.log("Headers");
      console.log(req.headers);

      console.log("Body");
      console.log(req.body);
    }
  )

/*en v1 vamos a trabajar con ficheros
app.get ("/apitechu/v1/users",
  function (req,res){
    console.log("GET /apitechu/v1/users");
    var users =require('./usuarios.json');
    res.send(users);
    //envío el fichero
    //res.sendfile("usuarios.json",{root: __dirname});
  }
)*/




//POST NECESITA DATOS DE ENTRADA: versión con parámetros en el header
/*
app.post ("/apitechu/v1/users",
  function (req,res){
    console.log("POST /apitechu/v1/users");
    console.log(req.headers);
    console.log(req.headers.first_name);
    console.log(req.headers.last_name);
    console.log(req.headers.email);

    var newUser ={
      "first_name": req.headers.first_name,
      "last_name": req.headers.last_name,
      "email": req.headers.email
    }
    console.log(newUser);
    //trabajamos por referencia
    var users =require('./usuarios.json');
    //añado algo al final de un array
    users.push(newUser);
    console.log("Usuario añadido al array");

    const fs = require('fs');

    var jsonUserData = JSON.stringify(users);
    fs.writeFile("./usuarios.json", jsonUserData,"utf8",
      function(err){
        if(err){
          console.log(err);
        }else{
          console.log("Usuario escrito en fichero");
        }
      }
  );*/

  //recibimos filtros
app.get ("/apitechu/v1/users",userController.getUsersV1);
app.get ("/apitechu/v2/users",userController.getUsersV2);
app.get ("/apitechu/v2/users/:id",userController.getUserByIdV2);
  //POST NECESITA DATOS DE ENTRADA: versión con parámetros en el body
app.post ("/apitechu/v1/users",userController.createUserV1);
app.post ("/apitechu/v2/users",userController.createUserV2);

app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);

app.post ("/apitechu/v1/login",authController.loginV1);
app.post ("/apitechu/v2/login",authController.loginV2);

app.post ("/apitechu/v1/logout/:id",authController.logoutV1);
app.post ("/apitechu/v2/logout/:id",authController.logoutV2);

app.get("/apitechu/v2/accounts/:user_id",accountController.getAccountByUserId);
