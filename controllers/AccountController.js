//importo el paquete
const io = require('../io');
const crypt = require('../crypt');
const requestJson =require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechummt12ed/collections/";
const mLabAPIKey ="apiKey=" + process.env.MLAB_API_KEY;





function getAccountByUserId(req,res){
  console.log("GET /apitechu/v2/accounts/:user_id");


  var userId = Number.parseInt(req.params.user_id);
  console.log("La id del usuario a obtener es: " + userId);


  var query ="q=" + JSON.stringify({"id_usuario": userId});
  var httpClient =requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
  function(err,resMLab, body) {
    // var response = !err ? body[0] :{
    //   "msg" : "Error obteniendo usuario "
    // }
    if(err){
      var response = {
        "msg": "Error obteniendo cuentas"
      }
      res.status(500);
    } else{
      if(body.length >0){
        var response = body;

      }else{
        var response ={
          "msg" : "Usuario no encontrado"
        }
        res.status(404);
      }
    }
    res.send(response);
  }
);

}





//para poder llamar a las fuciones desde otros ficheros

module.exports.getAccountByUserId=getAccountByUserId;
