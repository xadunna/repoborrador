//importo el paquete
const io = require('../io');
const crypt = require('../crypt');
const requestJson =require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechummt12ed/collections/";
const mLabAPIKey ="apiKey=" + process.env.MLAB_API_KEY;

function loginV1(req,res){
  console.log("POST /apitechu/v1/login");
  console.log(req.body);

//carmagamos usuarios
var users =require('../usuarios.json');
//buscamos al usuario

var indexUser=users.findIndex(function(user) {
  console.log("user:"+ user.email + "_" + user.password);
  return ((user.email == req.body.email)  && (user.password == req.body.password));
}
);


var respuesta={};
console.log("indexUser " + indexUser);
if(indexUser==-1){
  //No hay entrada para ese usuario
  respuesta.mensaje="Login incorrecto";

}
else{
  respuesta.mensaje="Login correcto"
  respuesta.idUsuario =users[indexUser].id;
  users[indexUser].logged=true;
  io.writeUserDataToFile(users);
}

res.send(respuesta);
}

function loginV2(req,res){
  console.log("POST /apitechu/v2/login");

  var email = req.body.email;
  console.log("El email del usuario a obtener es: " + email);
  var password = req.body.password;

  var httpClient =requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  //me aseguro que no metan código
  var query = "q=" + JSON.stringify({"email" : email});
  //var query ='q={"email":"' + email + '"}';
  console.log(query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err,resMLab, body) {
    if(body.length >0){
      //Compruebo la contraseña
      var response = body[0];
      var passwordEncripted=body[0].password;
      if(crypt.checkPassword(password,passwordEncripted) == true){
        console.log("Contraseña correcta");
        var id= Number.parseInt(body[0].id);
        query = "q=" + JSON.stringify({"id" : id});
        //POdemos haer la consulta por id en vez de por emial con un parseInt de los params y un stringify
        var putBody = '{"$set":{"logged":true}}';
        httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
        function(errPut,resMLabPut, bodyPut) {
          var response={
            "msg": "Login correcto",
            "id": body[0].id
          }
          res.send(response);
        }
      );

     }else{
       console.log("Contraseña inccorrecta");
       res.status(401);
       res.send({"msg" : "Login incorrecto"});
     }

    }else{
      res.status(401);
      res.send({"msg" : "Login incorrecto"});
    }


  }
);

}

function logoutV1(req,res){
  console.log("POST /apitechu/v1/logout");
  console.log(req.params);
  //carmagamos usuarios
  var users =require('../usuarios.json');
  //buscamos al usuario
  var indexUser=users.findIndex(function(user) {
      console.log("user:"+ user.id + "_" + user.logged);
    return ((user.id == req.params.id) && (user.logged === true));
  }
  );
  var respuesta={};
  console.log("indexUser " + indexUser);
  if(indexUser ==-1){
    //No hay entrada para ese usuario
    respuesta.mensaje="Logout incorrecto";

  }
  else{
    respuesta.mensaje="Logout correcto"
    delete users[indexUser].logged;
    io.writeUserDataToFile(users);
  }
  res.send(respuesta);
}

function logoutV2(req,res){
  console.log("POST /apitechu/v2/logout");
  console.log("El id del usuario es: " + req.params.id);

  var httpClient =requestJson.createClient(baseMLABUrl);
  console.log("Client created");
  var id = Number.parseInt(req.params.id);
   var query = "q=" + JSON.stringify({"id": id});
  var query ='q={"id":' + id + '}';
  console.log(query);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err,resMLab, body) {
    if(body.length >0 && body[0].logged === true){
      console.log("Deslogueamos al usuario");
      var putBody = '{"$unset":{"logged":""}}';
      httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
      function(errPut,resMLabPut, bodyPut) {
          res.send({"msg" : "Logout correcto"});
      }
      );
    }else{
      res.status(401);
      res.send({"msg" : "Logout incorrecto"});
    }
  }
);
}
//para poder llamar a las fuciones desde otros ficheros
module.exports.loginV1=loginV1;
module.exports.loginV2=loginV2;
module.exports.logoutV1=logoutV1;
module.exports.logoutV2=logoutV2;
