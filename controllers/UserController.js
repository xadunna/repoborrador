//importo el paquete
const io = require('../io');
const crypt = require('../crypt');
const requestJson =require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechummt12ed/collections/";
const mLabAPIKey ="apiKey=" + process.env.MLAB_API_KEY;



function getUsersV1(req,res){
  console.log("GET /apitechu/v1/users");
  console.log("Query String");
  console.log(req.query);
  var count=true;

  if (Number.parseInt(req.query.$top)!=NaN){
    console.log("top " + req.query.$top);
    var top=req.query.$top;
  }else{
    console.log("Sin top");
  }
  if (req.query.$count== "true") {
    //var count=req.query.$count;
    console.log("count " + req.query.$count);
  }else{
    count=false;
    console.log("Sin count");
  }
  //creo array
  var usersResult = {};

  var users =require('../usuarios.json');
  if(top){
    users=users.slice(0,top);
  }

  if(count){
  usersResult.count=users.length;
  }
  usersResult.users=users;
  res.send(usersResult);

}

function getUsersV2(req,res){
  console.log("GET /apitechu/v2/users");

  var httpClient =requestJson.createClient(baseMLABUrl);

  console.log("Client created");
  //
  httpClient.get("user?" + mLabAPIKey,
  function(err,resMLab, body) {
    var response = !err ? body :{
      "msg" : "Error obteniendo usuarios"
    }
    res.send(response);
  }
);
}

function getUserByIdV2(req,res){
  console.log("GET /apitechu/v2/users/:id");


  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a obtener es: " + id);


  var query ="q=" + JSON.stringify({"id": id});
  var httpClient =requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err,resMLab, body) {
    // var response = !err ? body[0] :{
    //   "msg" : "Error obteniendo usuario "
    // }
    if(err){
      var response = {
        "msg": "Error obteniendo usuario"
      }
      res.status(500);
    } else{
      if(body.length >0){
        var response = body[0];

      }else{
        var response ={
          "msg" : "Usuario no encontrado"
        }
        res.status(404);
      }
    }
    res.send(response);
  }
);

}

function createUserV1(req,res){
  console.log("POST /apitechu/v1/users");
  console.log(req.body);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser ={
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }
  console.log(newUser);
  //trabajamos por referencia
  var users =require('../usuarios.json');
  //añado algo al final de un array
  users.push(newUser);
  console.log("Usuario añadido al array");

  io.writeUserDataToFile(users);
  res.send({"msg":"Usuario creado"});
}

function createUserV2(req,res){
  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser ={
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
  }

  var httpClient =requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.post("user?" + mLabAPIKey,newUser,
  function(err,resMLab, body) {
    console.log("Usuario creado");
    res.status(201).send({"msg" : "Usuario creado"});
  }
);
}

/* Versión incial
function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");

  console.log("La id del usuario a borrar es " + req.params.id);

  var users =require('../usuarios.json');
  users.splice(req.params.id-1,1);
  io.writeUserDataToFile(users);
  res.send({"msg":"Usuario borrado"});
}*/

// Versión con bucle
function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");

  console.log("La id del usuario a borrar es " + req.params.id);

  var users =require('../usuarios.json');
  var i;
  var found= false;
  var user;
/*version con bucle for
  for (i=0;i<users.length;i++){
    if (users[i].id==req.params.id){
      console.log("i " + i);
      found =true;
      break;
    }
  }*/

  //i es el úndice en el array
/* Versión con bucle for in
  for (i in users){
    if (users[i].id == req.params.id){
      console.log("i " + i);
      found =true;
      break;
    }
  }*/

  /* Versión con bucle for each
  users.forEach(function(user,index) {
    if (user.id == req.params.id){
      found =true;
      i=index;
      console.log("index " + index);
    }
})*/
/* bucle con for of; se podría usar los pares obtenidos por la función entries for var [index,user] of user.entries()
i=0;
for (let user of users){
  if (user.id == req.params.id){
    console.log("i " + i);
    found =true;
    break;
  }else{
      i+=1;
  }
}
*/
// Versión con bucle find Index
i=users.findIndex(function(user,index) {
  if (user.id == req.params.id){
    found =true;
    console.log("index " + index);
    return true;
  }
})

  if (found){
    console.log("i " + i);
      users.splice(i,1);
      io.writeUserDataToFile(users);
      res.send({"msg":"Usuario borrado"});
  }else{
    res.send({"msg":"Usuario no encontrado"});
  }


}

//para poder llamar a las fuciones desde otros ficheros
module.exports.getUsersV1=getUsersV1;
module.exports.getUsersV2=getUsersV2;
module.exports.getUserByIdV2=getUserByIdV2;
module.exports.createUserV1=createUserV1;
module.exports.createUserV2=createUserV2;
module.exports.deleteUserV1=deleteUserV1;
