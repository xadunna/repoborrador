const mocha = require('mocha');
const chai  = require('chai');
const chaihttp = require('chai-http');
//para que podamos usar chait-http desde chai
chai.use(chaihttp);

//mecanismo para hacer aserciones
var should = chai.should();

describe("First test",
function() {
  //it -> test unitario
  it('Tests that duckduckgo works', function(done){
    chai.request('http://www.duckduckgo.com')
    .get('/')
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        // console.log(res);
        // console.log(err);
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
)
}
)

describe("Test de API de usuarios",
function() {
  //it -> test unitario
  it('Tests that user api says hello', function(done){
    chai.request('http://localhost:3000')
    .get('/apitechu/v1/hello')
    .end(
      function(err, res){
        // //función manejadora: es donde irán las aserciones
        console.log("Request finished");
        //chequeo el status de la respuesta
        res.should.have.status(200);
        //chequeo el contenido del mensaje
        res.body.msg.should.be.eql("Hola desde API TechU");
        done(); // momento en el que hay que comprobar las aserciones
      }
    )
  }
),
it('Tests that user api returns user list', function(done){
  chai.request('http://localhost:3000')
  .get('/apitechu/v1/users')
  .end(
    function(err, res){
      // //función manejadora: es donde irán las aserciones
      console.log("Request finished");
      //chequeo el status de la respuesta
      res.should.have.status(200);
      //chequeo que el objeto sea un array
      res.body.users.should.be.a('array');
      //Comprobamos la integridad del usuario
      for (user of res.body.users){
        user.should.have.property('id');
        user.should.have.property('email');
        user.should.have.property('password');
      }
      done(); // momento en el que hay que comprobar las aserciones
    }
  )
}
)
}
)































//Documento MongoDB
{
  "first_name" : "Sever",
  "last_name" : "iano",
  "quesos" : ["Dehesa de los Llanos", "Montescusa"],
  "amigotes" : [
    {
      "name" : "Manolo",
      "de_quien_es" : "el de Pistolas"
    },
    {
      "name" : "Indalecio",
      "de_quien_es" : "el de Pelala"
    }
  ],
  "queso_favorito" : {
    "name" : "Dehesa de los Llanos",
    "milk" : "cabra"
  }
}
